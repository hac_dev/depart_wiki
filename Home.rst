

Please use `reStructuredText`_ markup with this wiki.
You could however instead use `GitHub-flavoured markdown` 
If you want to use MoinMoin style markup instead, start your page with processing instruction '#format wiki'.

* more information on `this wiki`_

* `Tiger`_
* `Loopdrive`_
* projects_
* development_
* department_
* Hacousto_
* wtankink_

.. _`this wiki`: department/tools/wiki
.. _`Tiger`: projects/tiger
.. _`Loopdrive`: projects/loopdrive
.. _projects: projects
.. _`development`: development
.. _`department`: department
.. _Hacousto: Hacousto
.. _wtankink: wtankink
.. _`reStructuredText`: wtankink/technologies/rst
.. _`GitHub-flavoured markdown`: wtankink/technologies/github_markdown